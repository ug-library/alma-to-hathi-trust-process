# Some local, non repo notes & scribbles
Basic compose commands more detail in project README

## docker cmd
docker-compose build test_alma_hathitrust_app
docker-compose run test_alma_hathitrust_app

docker-compose build gen_alma_hathitrust_serials_app
docker-compose run gen_alma_hathitrust_serials_app

docker-compose build gen_alma_hathitrust_multi_app
docker-compose run gen_alma_hathitrust_multi_app

docker-compose build gen_alma_hathitrust_single_app
docker-compose run gen_alma_hathitrust_single_app

docker-compose rm