# Alma to Hathi Trust Process

This process will run in a container and takes files exported from an Alma Publishing Profile and processes those to produce files to import into Hathi Trust

It's meant to run a specific job for each HathiTrust type, serials, multi and single

## Steps for HathTrust files
1. ensure docker is running on machine
2. ensure this repository is on the machine

### Steps for serial
1. ensure no xml files exist in alma_xml_input
2. Move the Alma xml files for serials into alma_xml_input
3. In a terminal / cmd line, move into repository directory with the docker-compose.yml file
4. docker-compose build gen_alma_hathitrust_serials_app
5. docker-compose run gen_alma_hathitrust_serials_app
 
### Steps for multi
1. ensure no xml files exist in alma_xml_input
2. Move the Alma xml files for mult-parts into alma_xml_input
3. In a terminal / cmd line, move into repository directory with the docker-compose.yml file
4. docker-compose build gen_alma_hathitrust_multi_app
5. docker-compose run gen_alma_hathitrust_multi_app

### Steps for single
1. ensure no xml files exist in alma_xml_input
2. Move the Alma xml files for mult-parts into alma_xml_input
3. In a terminal / cmd line, move into repository directory with the docker-compose.yml file
4. docker-compose build gen_alma_hathitrust_single_app
5. docker-compose run gen_alma_hathitrust_single_app

### clean up
we want to remove unneeded images and tidy up after our process

1. docker-compose rm
