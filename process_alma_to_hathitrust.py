import configparser
import os
import sys
import logging.config
from datetime import datetime
import pytz
from lxml import etree as et, objectify

BASE_DIR = os.path.dirname(os.path.realpath(__file__))

# configure a bit of logging 
alma_hathi_log_file = f"{BASE_DIR}{os.sep}logs{os.sep}alma_hathi_remap_info.log"
exception_log_file = f"{BASE_DIR}{os.sep}logs{os.sep}exceptions.log"

LOGGING_CONFIG = None
LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'standard': {
            'format' : "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            'datefmt' : "%d/%b/%Y %H:%M:%S"
        },
    },
    'handlers': {
        'null': {
            'level':'DEBUG',
            'class':'logging.NullHandler',
        },                
        'alma_hathi_rotating_file': {
            'level' : 'INFO',
            'formatter' : 'standard',
            'class' : 'logging.handlers.RotatingFileHandler',
            'filename' : alma_hathi_log_file,
            'maxBytes' : 10000000,
            'backupCount' : 7,
        },          
        'exception_rotating_file': {
            'level' : 'INFO',
            'formatter' : 'standard',
            'class' : 'logging.handlers.RotatingFileHandler',
            'filename' : exception_log_file,
            'maxBytes' : 4000000,
            'backupCount' : 5,
        },          
        'console':{
            'level':'INFO',
            'class':'logging.StreamHandler',
            'formatter': 'standard'
        },
    },
    'loggers': {        
        'alma_hathi_logger': {
            'handlers': ['alma_hathi_rotating_file'],
            'level': 'INFO',
            'propagate': True,
        },        
        'exception_logger': {
            'handlers': ['exception_rotating_file'],
            'level': 'INFO',
            'propagate': True,
        },        
    }
}

# use a customTime for logging, ensure timestamps make sense within a container
def customTime(*args):
    '''
    setup logging timestamp to be eastern time
    '''
    utc_dt = pytz.utc.localize(datetime.utcnow())
    my_tz = pytz.timezone("US/Eastern")
    converted = utc_dt.astimezone(my_tz)
    return converted.timetuple()

logging.Formatter.converter = customTime

logging.config.dictConfig(LOGGING)

# instantiate logging
# load the logger this process will use
logger = logging.getLogger('alma_hathi_logger')
exception_logger = logging.getLogger('exception_logger')

# start with a new log file instead of continuous roll...more for dev / unique runs
# typically will start logging in the .1 file
handler = logging.handlers.RotatingFileHandler(alma_hathi_log_file, mode='w', backupCount=7)
handler.doRollover()

handler = logging.handlers.RotatingFileHandler(exception_log_file, mode='w', backupCount=5)
handler.doRollover()

def _get_xml_data(rec, field):
    '''
    extract the data from the xml objectify object used to build data
    '''
    
    val = ""
    try:
        val = rec[field]
    except:
        pass
    
    return str(val)

def buildSerialsRecord(rec):
    '''
    parse and construct the tags/fields for the HathiTrust serials record
    Serials (include data elements 1, 2, 6, 7):
    '''
    rec_list = []    
    issn_map = {}
    oclc = ""    
    controlfield = _get_xml_data(rec, "controlfield")

    # 6 is the tag 022 - ISSN 
    # iterate overchildren
    for e in rec.getchildren():
        try: 
            attrib = e.attrib.get("tag","")            
            # it's the issn - remove duplicates
            if (attrib == "022"):
                for sub in e.getchildren():
                    issn = sub.text
                    if len(issn) > 4:
                        # keep issn
                        issn_map[issn] = sub.attrib.get("code","")
            elif (attrib == "035"):
                for sub in e.getchildren():
                    oclc_tmp = sub.text
                    # eg  value: (OCoLC)226060459, (OCoLC)ocn226060459
                    # only keep the value that is all digits after )
                    # only want 1 oclc
                    oclc_list = oclc_tmp.split(")")
                    if oclc_list[1].isdigit():
                        oclc = oclc_tmp                                        
        except Exception as e:
            exception_logger.exception(e)

    rec_list.append(oclc)
    rec_list.append(controlfield)
    issn_hathi = ""
    for sub in issn_map:
        issn_hathi = f"{issn_hathi}{sub},"
    
    # if a value, strip last char off (comma)
    if len(issn_hathi) > 1:
        issn_hathi = issn_hathi[0:len(issn_hathi)-1]

    rec_list.append(issn_hathi)        
    rec_list.append("\n")

    return rec_list

def buildMultiRecord(rec):
    '''
    parse and construct the tags/fields for the HathiTrust multi-part record
    Multi-part monographs (include data elements 1, 2, 3, 4, 5, 7):
    '''
    rec_list = []        
    oclc = ""    
    controlfield = _get_xml_data(rec, "controlfield")
    
    # 7 is the tag 086 - Government Documents 
    # attribute ind1 1 - canadian, blank -> 0 US but we're not going to send to HathiTrust
    # iterate overchildren
    for e in rec.getchildren():
        try: 
            attrib = e.attrib.get("tag","")            
            # it's the oclc# we'll only take the all digits version
            if (attrib == "035"):
                for sub in e.getchildren():
                    oclc_tmp = sub.text
                    # eg  value: (OCoLC)226060459, (OCoLC)ocn226060459
                    # only keep the value that is all digits after )
                    # only want 1 oclc
                    oclc_list = oclc_tmp.split(")")
                    if oclc_list[1].isdigit():
                        oclc = oclc_tmp                                        
        except Exception as e:
            exception_logger.exception(e)

    rec_list.append(oclc)
    rec_list.append(controlfield)
            
    rec_list.append("")
    rec_list.append("")
    rec_list.append("")        
    rec_list.append("\n")

    return rec_list    

def buildSingleRecord(rec):
    '''
    parse and construct the tags/fields for the HathiTrust single-part record
    Single-part monographs (include data elements 1, 2, 3, 4, 7):
    '''
    rec_list = []        
    oclc = ""    
    controlfield = _get_xml_data(rec, "controlfield")
    
    # 7 is the tag 086 - Government Documents 
    # attribute ind1 1 - canadian, blank -> 0 US but we're not going to send to HathiTrust
    # iterate overchildren
    for e in rec.getchildren():
        try: 
            attrib = e.attrib.get("tag","")            
            # it's the oclc# we'll only take the all digits version
            if (attrib == "035"):
                for sub in e.getchildren():
                    oclc_tmp = sub.text
                    # eg  value: (OCoLC)226060459, (OCoLC)ocn226060459
                    # only keep the value that is all digits after )
                    # only want 1 oclc
                    oclc_list = oclc_tmp.split(")")
                    if oclc_list[1].isdigit():
                        oclc = oclc_tmp                                        
        except Exception as e:
            exception_logger.exception(e)

    rec_list.append(oclc)
    rec_list.append(controlfield)
            
    rec_list.append("")
    rec_list.append("")    
    rec_list.append("\n")    

    return rec_list    

def process_file(xml_path,fname, txt_hathi_output, output_fname,hathi_type):
    '''
    Actually read through the Alma xml file and perform the Alma to HathiTrust map
    Three different output format's / fields determine based on hathi_type which
    is a program arg (serial,single,multi)
    '''       
    record_count = 0
    empty_oclc = 0

    try:        
                
        # create the HathiTrust file        
        out_path = f"{txt_hathi_output}{output_fname}.txt"

        # create/append an empty temp file to use
        # ensure file doesn't exist before running
        with open(out_path, "a") as outfile:
            outfile.flush()
        
        # process the input file
        fpath = f"{xml_path}{fname}"
                 
        with open(fpath, 'rb') as fobj:
            xml = fobj.read()
            root_obj = objectify.XML(xml)            

            with open(out_path, "a") as outfile:                

                # info provided: https://www.hathitrust.org/print_holdings
                # Single-part monographs (include data elements 1, 2, 3, 4, 7):
                # Multi-part monographs (include data elements 1, 2, 3, 4, 5, 7):
                # Serials (include data elements 1, 2, 6, 7):
                for rec in root_obj.getchildren():
                    try: 
                        rec_list = []                        
                        record_count = record_count + 1                        
                        issn_map = {}
                        oclc = ""                                                

                        # controlfield goes second                                                                        
                        if hathi_type == "serial":
                            rec_list = buildSerialsRecord(rec)
                        elif hathi_type == "multi":
                            rec_list = buildMultiRecord(rec)
                        elif hathi_type == "single":
                            rec_list = buildSingleRecord(rec)
                        
                        # oclc is required and it's the first element of the list from the build functions
                        if len(rec_list) > 0:
                            oclc = rec_list[0]
                            if len(oclc) > 0:
                                # now seperate fields with a tab to create the tab delimited HathiTrust record
                                hathi_record = "\t".join(rec_list)
                                outfile.write(hathi_record)
                            else:
                                empty_oclc = empty_oclc + 1
                        else:
                            empty_oclc = empty_oclc + 1
                    except Exception as fe:
                        exception_logger.exception(fe)                                       

    except Exception as e:
        exception_logger.exception(e)
    
    return record_count, empty_oclc

if __name__ == "__main__":
    '''
    A script to process the xml files exported from alma to format into a txt file for hathitrust
    This is only run once in a while (once a year?), doesn't need to be automated
    '''    
    dt_end = None
    dt_start = None    
    eastern=pytz.timezone('America/Toronto')

    try:
        logger.info("********** RUNNING THE ARES TO HATHITRUST  MAP **********")
        dt_start = datetime.now(tz=eastern)           
        logger.info("sys args: " + str(sys.argv))

        # for each xml file in the alma_xml_input directory, process the xml record and create a tab delimited record for hathitrust        

        record_count = 0        
        total_record_count = 0                
        xml_file_count = 0
        empty_oclc = 0
        total_empty_oclc = 0
        
        if len(sys.argv) > 1:
            output_fname = sys.argv[1].split(' ')[0]            
            hathi_type = sys.argv[2].split(' ')[0]
        
        xml_path = f"{BASE_DIR}{os.sep}alma_xml_input{os.sep}"
        # Hathi output location
        txt_hathi_output = f"{BASE_DIR}{os.sep}txt_hathi_output{os.sep}"         
            
        if os.path.isdir(xml_path):                        
            # dir is found, get the xml files located in it
            dir_list = os.listdir(xml_path)
            for fname in dir_list:            
                if ".xml" in fname:
                    xml_file_count = xml_file_count + 1  
                    logger.info(f"Processing xml file: {xml_path}{fname}")
                    record_count, empty_oclc = process_file(xml_path,fname,txt_hathi_output,output_fname,hathi_type)
                    logger.info(f"{record_count} Alma records processed from file {fname}")
                    logger.info(f"{empty_oclc} EMPTY OCLC records processed and NOT INCLUDED from file {fname}")
                    logger.info(f"{record_count - empty_oclc} records included in HathiTrust file from file {fname}")
                    total_record_count = total_record_count + record_count  
                    total_empty_oclc = total_empty_oclc + empty_oclc
        else:           
            logger.info("XML input path not found")
                
        logger.info(f"{total_record_count} TOTAL records processed from all xml input files")
        logger.info(f"{total_empty_oclc} TOTAL EMPTY OCLC records processed and NOT INCLUDED from all xml input files")
        logger.info(f"{total_record_count - total_empty_oclc} records included in HathiTrust file")

        dt_end = datetime.now(tz=eastern)

        #determine the time delta from start to finish
        delta = (dt_end - dt_start).total_seconds()
    
        logger.info(f"Number of xml files processed: {xml_file_count}")
        logger.info(f"********** ARES TO HATHITRUST MAP PROCESS OVER AT: {dt_end} **********")        

    except Exception as e:
        exception_logger.exception(e)


