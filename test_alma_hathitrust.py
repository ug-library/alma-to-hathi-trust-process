import os
import pytest
import configparser

BASE_DIR = os.path.dirname(os.path.realpath(__file__))

def test_alma_data_files():
    """
    ensure the datafiles are seen within container, checking the alma_xml_input directory
    """
    result = False
    
    fpath = f"{BASE_DIR}{os.sep}alma_xml_input{os.sep}"    
    try:       
        if os.path.isdir(fpath):
            # dir is found, get the csv files located in it
            dir_list = os.listdir(fpath)
            for fname in dir_list:            
                if ".xml" in fname:
                    # found one, we can stop
                    result = True
                    break
    except Exception:
        pass    

    assert result, "No xml files located in alma_xml_input directory"



